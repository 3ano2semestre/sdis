import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashMap;


public class Server {

	static HashMap<String, String> matriculas = new HashMap<String, String>();
	/**
	 * @param args
	 */
	public static void main(String[] args){

		if (args.length != 1) {
			System.out.println("Usage: java Server <port>");
		}
		try{
			String port = args[0];
			// create socket
			DatagramSocket socket = new DatagramSocket(Integer.parseInt(port));
			while(true){
				byte[] rbuf = new byte[Utils.MESSAGE_SIZE];
				DatagramPacket packet = new DatagramPacket(rbuf, rbuf.length);
				socket.receive(packet);
				
				String received = new String(packet.getData());
				System.out.println("A client sent: " + received);
				
				String[] rsplit = received.split("\\s+");
				for(int i = 0; i < rsplit.length; i++){
					rsplit[i] = rsplit[i].trim();
				}
				
				String message = "";
				
				System.out.println("rsplit[0]: " + rsplit[0]);
				
				if(rsplit[0].compareTo("lookup") == 0){
					String matricula = rsplit[1];
					String dono = matriculas.get(matricula);
					if(dono == null)
						message += "NOT_FOUND";
					else
						message += matricula + " " + dono;
				}
				else if(rsplit[0].compareTo("register") == 0){
					String matricula = rsplit[1];
					String dono = rsplit[2];
					matriculas.put(matricula, dono);
					message = "success";
				}
				else message = "-1";
				InetAddress clientAddress = packet.getAddress();
				int clientPort = packet.getPort();
				
				byte[] sbuf = message.getBytes();
				packet = new DatagramPacket(sbuf, sbuf.length, clientAddress, clientPort);
				socket.send(packet);
			}
	        //socket.close();
		} catch(Exception e){
			
		}

	}

}
