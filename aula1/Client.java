import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Client {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		if (args.length < 4) {
			System.out.println("Usage: java Client <host_name> <port_number> <oper> <opnd>*");
		}
		
		DatagramSocket socket = new DatagramSocket();
		String hostname = args[0];
		String port = args[1];
		
		String message = "";
		for(int i = 2; i < args.length; i++){
			message += args[i] + " ";
		}
		
		byte[] sbuf = message.getBytes();
		
		InetAddress address = InetAddress.getByName(hostname);
		DatagramPacket packet = new DatagramPacket(sbuf, sbuf.length,address, Integer.parseInt(port));
		socket.send(packet);
		

		byte[] rbuf = new byte[Utils.MESSAGE_SIZE];
		DatagramPacket answerPacket = new DatagramPacket(rbuf, rbuf.length);
		socket.receive(answerPacket);
		String received = new String(answerPacket.getData());
		
		System.out.println("Reply: " + received);
	}

}
