import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

public class Client {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws NumberFormatException 
	 */
	public static void main(String[] args) throws NumberFormatException, IOException {
		/*if (args.length < 4) {
			System.out.println("Usage: java Client <srv_addr> <srv_port> <oper> <opnd> * ");
		}*/
		String hostname = args[0];
		String port = args[1];
		
		String message = "";
		for(int i = 2; i < args.length; i++){
			message += args[i] + " ";
		}
		System.out.println(message);
		
		InetAddress address = InetAddress.getByName(hostname);
		Socket client = new Socket(address, Integer.parseInt(port));
		PrintWriter out = new PrintWriter(client.getOutputStream(), true);
		out.println(message);
		BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
		System.out.println(in.readLine());
		
		out.close();
		client.close();
	}

}
