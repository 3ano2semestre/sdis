import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;


public class Server {
	
	static HashMap<String, String> matriculas = new HashMap<String, String>();
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		ServerSocket srvSocket = new ServerSocket(2011);
		System.out.println("Listening on port 2011");
		while(true){
			Socket s = srvSocket.accept();
			BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			String received = in.readLine();
			PrintWriter out = new PrintWriter(s.getOutputStream(), true);
			
			System.out.println(received);
			
			String[] rsplit = received.split("\\s+");
			for(int i = 0; i < rsplit.length; i++){
				rsplit[i] = rsplit[i].trim();
			}
			
			String message = "";
			if(rsplit[0].compareTo("lookup") == 0){
				String matricula = rsplit[1];
				String dono = matriculas.get(matricula);
				if(dono == null)
					message += "NOT_FOUND";
				else
					message += matricula + " " + dono;
			}
			else if(rsplit[0].compareTo("register") == 0){
				String matricula = rsplit[1];
				String dono = rsplit[2];
				matriculas.put(matricula, dono);
				message = "success";
			}
			else message = "-1";
			
			out.println(message);
			s.close();
		}	
	}
}
