import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class Client {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		if (args.length < 4) {
			System.out.println("Usage: java Client <mcast_addr> <mcast_port> <oper> <opnd> * ");
		}
		
		
		String hostname = args[0];
		String mcastPort = args[1];
		
		byte[] mbuf = new byte[Utils.MESSAGE_SIZE];
		DatagramPacket packet = new DatagramPacket(mbuf, mbuf.length);
		
		MulticastSocket socket = new MulticastSocket(Integer.parseInt(mcastPort));
		socket.joinGroup(InetAddress.getByName(hostname));
		
		System.out.println("Client waiting for server address");
		socket.receive(packet);
		
		String multiMessage = new String(packet.getData());
		System.out.println("Received multicast message: " + multiMessage);
		
		String[] serverLocation = multiMessage.split(":");
		for(int i = 0; i < serverLocation.length; i++){
			serverLocation[i] = serverLocation[i].trim();
		}
		InetAddress address = InetAddress.getByName(serverLocation[0]);
		int port = Integer.parseInt(serverLocation[1]);
		
		String message = "";
		for(int i = 2; i < args.length; i++){
			message += args[i] + " ";
		}
		
		System.out.println("Sending command: " + message);
		
		DatagramSocket dataSocket = new DatagramSocket();
		byte[] sbuf = message.getBytes();
		DatagramPacket dataPacket = new DatagramPacket(sbuf, sbuf.length,address, port);
		dataSocket.send(dataPacket);
		
		byte[] rbuf = new byte[Utils.MESSAGE_SIZE];
		DatagramPacket answerPacket = new DatagramPacket(rbuf, rbuf.length);
		dataSocket.receive(answerPacket);
		String received = new String(answerPacket.getData());
		
		System.out.println("Reply: " + received);
	}

}
