import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;


public class Server{
	
	static String mcastAddr;
	static String srvcPort;
	static String mcastPort;

	static HashMap<String, String> matriculas = new HashMap<String, String>();
	/**
	 * @param args
	 * @throws IOException 
	 * @throws UnknownHostException 
	 */
	public static void main(String[] args) throws UnknownHostException, IOException {
		// TODO Auto-generated method stub
		if (args.length != 3) {
			System.out.println("Usage: java Server <srvc_port> <mcast_addr> <mcast_port>");
		}
		srvcPort = args[0];
		mcastAddr = args[1];
		mcastPort = args[2];
		
		MulticastNotice mn = new MulticastNotice();
		CarServiceWorker csw = new CarServiceWorker();
		mn.start();
		csw.start();
		
		
	}
	
	public static class MulticastNotice extends Thread{
		public void run(){
			try {
				System.out.println("Starting thread");
				InetAddress iAddr = InetAddress.getByName(mcastAddr);
				//MulticastSocket socket = new MulticastSocket();
				DatagramSocket socket = new DatagramSocket();
				
				//socket.joinGroup(iAddr);
				//socket.setTimeToLive(100);
				byte[] sbuf = new byte[Utils.MESSAGE_SIZE];
				sbuf = ("127.0.0.1:"+srvcPort).getBytes();
				while(true){
					sleep(1000);					
					DatagramPacket packet = new DatagramPacket(sbuf, sbuf.length, iAddr, Integer.parseInt(mcastPort));
					socket.send(packet);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
	public static class CarServiceWorker extends Thread{
		
		public void run(){
			System.out.println("Starting CarServiceWorker"); 
			try {
				DatagramSocket socket = new DatagramSocket(Integer.parseInt(srvcPort));
				byte[] rbuf = new byte[Utils.MESSAGE_SIZE];
				
				while(true){
					DatagramPacket packet = new DatagramPacket(rbuf, rbuf.length);
					socket.receive(packet);
					String received = new String(packet.getData());
					System.out.println("A client sent: " + received);
					
					String[] rsplit = received.split("\\s+");
					for(int i = 0; i < rsplit.length; i++){
						rsplit[i] = rsplit[i].trim();
					}
					
					String message = "";
					
					
					if(rsplit[0].compareTo("lookup") == 0){
						String matricula = rsplit[1];
						String dono = matriculas.get(matricula);
						if(dono == null)
							message += "NOT_FOUND";
						else
							message += matricula + " " + dono;
					}
					else if(rsplit[0].compareTo("register") == 0){
						String matricula = rsplit[1];
						String dono = rsplit[2];
						matriculas.put(matricula, dono);
						message = "success";
					}
					else message = "-1";
					
					System.out.println("Server answer: " + message);
					
					InetAddress clientAddress = packet.getAddress();
					int clientPort = packet.getPort();
					
					byte[] sbuf = message.getBytes();
					packet = new DatagramPacket(sbuf, sbuf.length, clientAddress, clientPort);
					socket.send(packet);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}

}
