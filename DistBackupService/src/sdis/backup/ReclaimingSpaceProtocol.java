package sdis.backup;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;



public class ReclaimingSpaceProtocol {


    public static void hasFile(BackupFile bf, int chunkNo){

        if (bf.getCopies(chunkNo) < bf.getRepDeg()) {
            ReclaimingSpaceWorker pw = new ReclaimingSpaceWorker(bf, chunkNo);
            pw.start();
        }
    }

    public static long getFolderSize(File backupFolder) {
        long size = 0;
        if (backupFolder.isDirectory()) {
            for (File file : backupFolder.listFiles()) {
                if (file.isFile()) {
                    size += file.length();
                } else
                    size += getFolderSize(file);
            }
        } else if (backupFolder.isFile()) {
            size += backupFolder.length();
        }
        return size;
    }

    public static long reclaiming(Integer size, File backupFolder){
        long spaceRecovered = 0;
        Collection<BackupFile> allFiles = App.getAllFiles();

        for(BackupFile file : allFiles) {
            ArrayList<Chunk> chunksForRemoved = file.getChunkForRemove();
            for(Chunk chunk : chunksForRemoved){
                App.mcChannel.sendRemovedChunkMessage(file.getUniqueId(), chunk.getChunkNo());
                File removedFile = new File(App.backupFolder.getAbsolutePath() + "/backup/" + file.getUniqueId() + '-' + chunk.getChunkNo());
                spaceRecovered += removedFile.length();
                removedFile.delete();
                chunk.decrementCopies();
                if (spaceRecovered >= size)
                    return spaceRecovered;

                Utils.sleep(400);
            }
        }

        return -1;
    }


    private static class ReclaimingSpaceWorker extends Thread{
        private BackupFile bf;
        private int chunkNo;

        public ReclaimingSpaceWorker(BackupFile bf, int chunkNo){
            this.bf = bf;
            this.chunkNo = chunkNo;
        }

        public void sendPutChunk(){
            byte[] chunk = RestoreProtocol.getChunk(bf.getUniqueId(), chunkNo);
            if(chunk != null){
                byte[] message = BackupProtocol.generatePutchunk(chunk, bf.getUniqueId(), chunkNo, bf.getRepDeg());
                App.mdbChannel.sendChunk(message);
            }
        }

        synchronized public void run(){

               Random r = new Random();
               long interval = Math.abs(400);
               Utils.sleep(interval);

               if (App.version.compareTo("1.0") == 0) {
                   if (bf.getCopies(chunkNo) < bf.getRepDeg()) {
                       sendPutChunk();
                   }
               }else if (App.version.compareTo("2.0") == 0) {
                   while(bf.getCopies(chunkNo) < bf.getRepDeg()){
                       sendPutChunk();
                       Utils.sleep(400);
                   }
               }
        }
    }
}
