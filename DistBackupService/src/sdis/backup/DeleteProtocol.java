package sdis.backup;

import java.io.File;
import java.util.Collection;


public class DeleteProtocol {


    public static void deleteFiles(BackupFile bf){
        DeleteWorker pw = new DeleteWorker(bf);
        pw.start();
    }

    private static class DeleteWorker extends Thread{
        private BackupFile bf;

        public DeleteWorker(BackupFile bf){
            this.bf = bf;
        }

        synchronized public void run(){

            //if(App.version.compareTo("1.0") == 0) {

                Collection<Chunk> chunks = bf.getChunkArray();

                for(Chunk c : chunks){
                    if(c.getStored()){
                        File file = new File(App.backupFolder.getAbsolutePath() + "/backup/" + bf.getUniqueId() + '-' + c.getChunkNo());
                        file.delete();
                        c.decrementCopies();
                        c.setStored(false);
                        RestoreProtocol.chunkMessages.remove(bf.getUniqueId() + '-' + c.getChunkNo());
                    }
                }
            //}
        }
    }
}
