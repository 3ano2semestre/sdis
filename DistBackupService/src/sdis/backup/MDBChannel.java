package sdis.backup;

import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

public class MDBChannel extends Thread{

	/**
	 * @param args
	 */
	private String mdbAddr;
	private String mdbPort;

	public static MulticastSocket mdbSocket;

	public MDBChannel(String mdbAddr, String mdbPort){
		this.mdbAddr = mdbAddr;
		this.mdbPort = mdbPort;

		try {
			mdbSocket = new MulticastSocket(Integer.parseInt(this.mdbPort));
			mdbSocket.setLoopbackMode(true);
			mdbSocket.joinGroup(InetAddress.getByName(this.mdbAddr));
            mdbSocket.setTimeToLive(1);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendChunk(byte[] message){
		DatagramPacket dataPacket;
		try {

			dataPacket = new DatagramPacket(message, message.length,InetAddress.getByName(mdbAddr),Integer.parseInt(mdbPort));
			mdbSocket.send(dataPacket);
			//fazer contagem do numero de ack recebidos para saber se precisa de enviar novamente o pedido.
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void run(){
		while(true){
			byte[] buf = new byte[70000];
			DatagramPacket packet = new DatagramPacket(buf, buf.length);
			try {
				mdbSocket.receive(packet);
                byte[] data = new byte[packet.getLength()];
                System.arraycopy(packet.getData(), 0, data, 0, packet.getLength());

                String received = new String(data);

				File folder = new File(App.backupFolder.getAbsolutePath() + "/backup/");

                if (((ReclaimingSpaceProtocol.getFolderSize(folder) + 64000) < App.folderSize) || App.folderSize == 0 )
                    BackupProtocol.parseMessage(data);

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}


}
