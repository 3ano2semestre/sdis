package sdis.backup;

public class Utils {
    public static boolean sleep(int ms){
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            return false;
        }
        return true;
    }
    public static boolean sleep(long ms){
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            return false;
        }
        return true;
    }
}
