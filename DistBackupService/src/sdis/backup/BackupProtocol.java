package sdis.backup;

import java.io.*;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import sdis.backup.BackupFile;
public class BackupProtocol{

    final public static int CHUNK_LENGTH = 64000;

    final private static Pattern PUTCHUNK_PATTERN = Pattern.compile("^PUTCHUNK (\\d\\.\\d) (.*) (\\d+) (\\d+)" );//\r\n\r\n(.*)


    public static void sendFile(BackupFile bf) throws IOException{
        InputStream is = new FileInputStream(bf.getFile());

        long chunkByteCount = 0;

        byte[] dataChunk = new byte[CHUNK_LENGTH];

        int b;
        int chunkNo = 0;
        int c;
        while((b = is.read(dataChunk)) != -1){
            byte[] resultChunk = new byte[b];
            System.arraycopy(dataChunk, 0, resultChunk, 0, b);
            chunkByteCount += b;
            byte[] message = generatePutchunk(resultChunk, bf.getUniqueId(), chunkNo, bf.getRepDeg());


            c = 1;
            while(bf.getCopies(chunkNo) < bf.getRepDeg()  && c <= 5){
                Utils.sleep(400*c);
                App.mdbChannel.sendChunk(message);
                c++;

            }
            chunkNo++;
        }

        if(b == CHUNK_LENGTH){
            byte[] message = generatePutchunk(new byte[1], bf.getUniqueId(), chunkNo, bf.getRepDeg());
            c = 1;
            while(bf.getCopies(chunkNo) < bf.getRepDeg()  && c <= 5){
                Utils.sleep(400*c);
                App.mdbChannel.sendChunk(message);
                c++;

            }
        }
        System.out.println("Backup de " + bf.getName() + " efectuado.");
        is.close();
    }

    public static void saveChunk(byte[] chunk, int size, String uID, int chunkNo){

        BackupFile f = App.getBackupFile(uID);

        f.setStored(chunkNo);

        FileOutputStream out;
        try {
            out = new FileOutputStream(App.backupFolder.getAbsolutePath() + "/backup/" + uID + '-' +chunkNo);
            out.write(chunk,0,size);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static class PutchunkWorker extends Thread{
        byte[]content;
        int chunkNo;
        BackupFile bf;
        public PutchunkWorker(byte[]content, BackupFile bf, int chunkNo){
            this.content = content;
            this.bf = bf;
            this.chunkNo = chunkNo;
        }
        synchronized public void run(){

            if(App.version.compareTo("2.0") == 0) {
                Random r = new Random();
                long interval = Math.abs(r.nextInt() % 400);
                Utils.sleep(interval);
            }
            if(bf.getCopies(chunkNo) < bf.getRepDeg()){
                //System.out.println("Interval: " + interval + "Enviar ack, chunk number: " + chunkNo + " - copies: " + bf.getCopies(chunkNo) + " rep deg = " + bf.getRepDeg());
                saveChunk(content, content.length, bf.getUniqueId(), chunkNo);
                App.mcChannel.sendReceivedAck(bf.getUniqueId(), chunkNo);
            }
        }
    }

    public static synchronized boolean parseMessage(byte[] m){

        InputStream is = new ByteArrayInputStream(m);
        BufferedReader bfReader = new BufferedReader(new InputStreamReader(is));
        String header = null;
        try {
            header = bfReader.readLine();
        } catch (IOException e) {
            return false;
        }
        byte[] content = new byte[m.length - header.length() - 4];
        System.arraycopy(m, header.length() + 4, content, 0, m.length - header.length() - 4);

        Matcher matcher = PUTCHUNK_PATTERN.matcher(header);
        if(matcher.matches()){
            String version = matcher.group(1);
            String fileId  = matcher.group(2);
            String chunkNo = matcher.group(3);
            String repDeg  = matcher.group(4);

            //System.out.println("PUTCHUNK CONTENT SIZE: " + content.length);
            BackupFile bf = App.getBackupFile(fileId);
            File chunk = new File(App.backupFolder.getAbsolutePath() + "/backup/" + fileId + '-' +chunkNo);

            if(bf == null){
                bf = new BackupFile(fileId, Integer.parseInt(repDeg));
                App.saveBackupFile(bf);
            }
            if(!bf.hasCompleteFile() && !chunk.exists()){
                PutchunkWorker pw = new PutchunkWorker(content, bf, Integer.parseInt(chunkNo));
                pw.start();
            }
            return true;
        }
        else{
            return false;
        }

    }

    public static byte[] generatePutchunk(byte[] chunk, String uID, int chunkNo, int repDegree){
        String header = "PUTCHUNK " + App.version + " " + uID + " " + chunkNo + " " + repDegree + "\r\n\r\n";
        byte[] headerB = header.getBytes();
        byte[] result = new byte[headerB.length + chunk.length];
        System.arraycopy(headerB, 0, result, 0, headerB.length);
        System.arraycopy(chunk, 0, result, headerB.length, chunk.length);
        return result;
    }
}
