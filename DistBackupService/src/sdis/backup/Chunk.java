package sdis.backup;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Emanuelpinho on 28/03/14.
 */
public class Chunk implements Serializable{

    /**
     * If file stored
     */
    private Boolean stored = false;


    /**
     * Number of copies
     */
    private int numCopies = 0;


    /**
     * Chunk Number
     */
    private int chunkNo;


    /**
     *
     */
    private ArrayList<String> ports;

    public Chunk(int chunkNo){
        this.chunkNo = chunkNo;
    }

    public synchronized void incrementCopies(){
        this.numCopies++;
    }
    public synchronized void decrementCopies(){
        this.numCopies--;
    }
    public synchronized void setNumCopies(int c){
        this.numCopies = c;
    }
    public int getNumCopies(){
        return this.numCopies;
    }
    public synchronized void setStored(Boolean stored){
        this.stored = stored;
    }
    public Boolean getStored(){
        return stored;
    }
    public int getChunkNo(){
        return chunkNo;
    }

}
