package sdis.backup;

import com.sun.xml.internal.xsom.impl.scd.Iterators;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sdis.backup.BackupFile;

public class App {

	public static final float PROTOCOL_VERSION = 1.0f;

	/**
	 * Folder handled by the application for creating and storing backups
	 */
	public static File backupFolder;


    /**
     * Version of app
     */
    public static String version;

    /**
     * Replication degree
     */
    private static int repDegree;

    /**
     * Size of Backup Folder
     */
    public static long folderSize;

    /**
     * Unique name of user
     */
    private static String username;

    /**
     *
     */
    final private static String STATE_FILE = "state.s";
	/**
	 * Multicast channels: mc = multicast channel; mdb = multicast data backup; mdr = multicast data restore
	 */
	private static String mcAddr;
	private static String mcPort;
	private static String mdbAddr;
	private static String mdbPort;
	private static String mdrAddr;
	private static String mdrPort;

    final private static Pattern RESTORE_PATTERN = Pattern.compile("^RESTORE (.*)" );//\r\n\r\n(.*)
    final private static Pattern DELETE_PATTERN = Pattern.compile("^DELETE (.*)" );//\r\n\r\n(.*)


    /**
	 * Stores the info about all backed up files
	 */
	private static HashMap<String, BackupFile> files = new HashMap<String, BackupFile>();

	/**
	 * Thread that checks for new and modified files
	 */
	private static ScheduledFolderWorker fWorker = new ScheduledFolderWorker();

	public static MCChannel mcChannel;
	public static MDRChannel mdrChannel;
	public static MDBChannel mdbChannel;


	/**
	 * @param args
	 */
	public static void main(String[] args) {

		if (args.length != 9) {
			System.out.println("Usage: java sdis.backup.App <multicast control address> <multicast control port> <multicast data backup address> <multicast data backup port> <multicast data restore address> <multicast restore port> <backup folder> <replication degree> <username>"); // adicionar channels multicast
			return;
		}

        //java sdis.backup.App 224.0.0.15 1200 224.0.0.15 1201 224.0.0.15 1202 ../../testfolder/PC2/ REPDGREE USERNAMe


		mcAddr = args[0];
		mcPort = args[1];
		mdbAddr = args[2];
		mdbPort = args[3];
		mdrAddr = args[4];
		mdrPort = args[5];
		backupFolder = new File(args[6]);
        repDegree = Integer.parseInt(args[7]);
        username = args[8];

        new File(backupFolder.getAbsolutePath() + "/backup").mkdir();

        load();

        folderSize = 0;

        version = "1.0";

		createChannels();
		fWorker.start();

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        String input = "";

        Matcher restoreMatcher, deleteMatcher;

        while(input.compareTo("exit") != 0){
            try {
                input = bufferRead.readLine();
                restoreMatcher = RESTORE_PATTERN.matcher(input);
                if(restoreMatcher.matches()){

                    String fileName = restoreMatcher.group(1);
                    String fileUID = BackupFile.makeUniqueId(fileName);
                    if(files.containsKey(fileUID)){
                        System.out.println("Restaurando " + fileName);
                         RestoreProtocol.restoreFile(App.getBackupFile(fileUID),0);
                    }
                    else{
                        System.out.println("Ficheiro desconhecido");
                    }
                }
                else if(input.compareTo("RECLAIMING SPACE") == 0){
                    File folder = new File(App.backupFolder.getAbsolutePath() + "/backup/");
                    folderSize = ReclaimingSpaceProtocol.getFolderSize(folder);

                    System.out.println("Atualmente a pasta de Backup tem " + folderSize + "bytes.\n" + "Qual o valor(em bytes) que deseja diminuir?");
                    input = bufferRead.readLine();
                    long spaceRecovered = ReclaimingSpaceProtocol.reclaiming(Integer.parseInt(input), folder);
                    if (spaceRecovered < 0)
                        System.out.println("ERROR! Sem chunks armazenados");
                    else
                        System.out.println("Ganhou " + spaceRecovered + "bytes.");

                    folderSize -= spaceRecovered;
                }
                else if(input.compareTo("LIST") == 0){
                    App.listLocalFiles();
                }
                else if(input.compareTo("SAVE") == 0 || input.compareTo("exit") == 0){
                    App.save();
                }
                deleteMatcher = DELETE_PATTERN.matcher(input);
                if(deleteMatcher.matches()){
                    String fileName = deleteMatcher.group(1);
                    String fileUID = BackupFile.makeUniqueId(fileName);
                    if(files.containsKey(fileUID)){
                        if (files.get(fileUID).hasCompleteFile()) {
                            System.out.println("Apagando " + fileName);
                            files.get(fileUID).getFile().delete();

                            mcChannel.sendDeleteMessage(fileUID);
                        }
                    }
                    else{
                        System.out.println("Ficheiro desconhecido");
                    }
                }

            } catch(IOException e) {
                e.printStackTrace();
            }
        }
        /*mdrChannel.interrupt();
        mdbChannel.interrupt();
        mcChannel.interrupt();
        */
	}

	private static void createChannels(){

		mcChannel = new MCChannel(mcAddr, mcPort);
		mcChannel.start();

		mdbChannel = new MDBChannel(mdbAddr, mdbPort);
		mdbChannel.start();

		mdrChannel = new MDRChannel(mdrAddr, mdrPort);
		mdrChannel.start();
	}

	public static class ScheduledFolderWorker extends Thread{
		public void run(){
			while(true){
				processFiles(backupFolder);
                //verifyFiles(backupFolder);  //Funcao que vê se ficheiros ainda estão na pasta para remover
				try {
					sleep(10000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

        public static void verifyFiles(final File folder){
            for(BackupFile file : files.values()){
                if(file.getFile() != null){
                    if (!searchFile(file.getName(), folder)) {
                        mcChannel.sendDeleteMessage(file.getUniqueId());
                    }
                }
            }
        }

        public static Boolean searchFile(String fileName, final File folder){
            for (final File fileEntry : folder.listFiles()) {
                if (fileEntry.isDirectory()) {
                    if (fileEntry.getName().matches("(.)*backup") == false) {
                        processFiles(fileEntry);
                    }
                } else {
                    if (fileEntry.getName().compareTo(fileName) == 0) {
                        return true;
                    }
                }
            }
            return false;
        }

		public static void processFiles(final File folder) {
		    for (final File fileEntry : folder.listFiles()) {
		        if (fileEntry.isDirectory()) {
		        	if(fileEntry.getName().matches("(.)*backup") == false){
		        		processFiles(fileEntry);
		        	}
		        } else {
		            //System.out.println(fileEntry.getName());
	            	BackupFile f = new BackupFile(fileEntry);
					String uID = f.getUniqueId();
					if(files.containsKey(uID)){
						if(fileEntry.lastModified() > files.get(uID).getLastRecordedModified()){
                            mcChannel.sendDeleteMessage(uID);
                            Utils.sleep(500);
                            try {
                                BackupProtocol.sendFile(f);
                            }
                            catch (IOException e) {
                            }
						}
					}
					else {
						try {

                            f.setRepDeg(App.repDegree);

                            files.put(uID, f);
                            System.out.println("Iniciando backup de " + f.getName());
							BackupProtocol.sendFile(f);
						} catch (IOException e) {
						}
					}

		        }
		    }
		}
	}


	public static BackupFile getBackupFile(String uID){
        return files.get(uID);
    }
    public static Collection<BackupFile> getAllFiles(){return files.values();}

    public static void saveBackupFile(BackupFile bf){
        files.put(bf.getUniqueId(), bf);
    }

    public static void removeBackupFile(String uID){
        files.remove(uID);
    }
    public static boolean hasLocalFile(String uID){
         if(files.containsKey(uID)){
             return files.get(uID).hasCompleteFile();
         }
        else return false;
    }

    public static void listLocalFiles(){
        ArrayList<BackupFile> arrBf = new ArrayList<BackupFile>(files.values());
        for(BackupFile f : arrBf){
            if(f.hasCompleteFile())
                System.out.println(f.getName());
        }
    }

    private static void save(){
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(backupFolder.getAbsolutePath() + "/backup/" + STATE_FILE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(files);
            oos.close();
        } catch (FileNotFoundException e) {
        }
        catch (IOException e) {
        };

    }

    private static boolean load(){
        boolean result = true;
        try {
            FileInputStream fis = new FileInputStream(backupFolder.getAbsolutePath() + "/backup/" + STATE_FILE);
            ObjectInputStream ois = new ObjectInputStream(fis);
            files = (HashMap<String,BackupFile>) ois.readObject();
        } catch (FileNotFoundException e) {
            result = false;
        }
        catch (IOException e) {
            result = false;
        } catch (ClassNotFoundException e) {
            result = false;
        }
        return result;
    }

    public static String getUsername(){
        return App.username;
    }
}
