package sdis.backup;

import java.io.*;
import java.util.HashMap;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RestoreProtocol {
	final public static int CHUNK_LENGTH = 64000;

    final private static Pattern CHUNK_PATTERN = Pattern.compile("^CHUNK (\\d\\.\\d) (.*) (\\d+)");

    private static int chunkNo;

    public static HashMap<String, Boolean> chunkMessages = new HashMap<String, Boolean>();

    public static void restore(String uid){
		try {
			FileOutputStream out = new FileOutputStream(App.backupFolder.getAbsolutePath() + "/restoredFile.txt");
			
			int chunkNo = 0;
			byte[] dataChunk = getChunk(uid,chunkNo);
			while(dataChunk != null){
				System.out.println("Leu: " + dataChunk.length);
				out.write(dataChunk,0,dataChunk.length);
				chunkNo ++;
				dataChunk = getChunk(uid,chunkNo);
			}
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}

	}

    public static void restoreFile(BackupFile bf, int chunkNo){

        //App.mdrChannel.sendGetChunk(bf.getUniqueId(), chunkNo);
        if(chunkNo == 0)
            RestoreProtocol.chunkNo = 0;
        App.mcChannel.sendGetChunk(bf.getUniqueId(), chunkNo);

        ///GETCHUNK <Version> <FileId> <ChunkNo><CRLF><CRLF>

    }

    /**
     * Get chunk file if it is stored locally
     * @param uid
     * @param chunkNo
     * @return byte[] data
     */
	public static byte[] getChunk(String uid, int chunkNo){
		byte[] dataChunk = null;
		try {
			InputStream is;
			is = new FileInputStream(App.backupFolder.getAbsolutePath() + "/backup/" + uid + '-' + chunkNo);

            File chunk = new File(App.backupFolder.getAbsolutePath() + "/backup/" + uid + '-' + chunkNo);
            dataChunk =  new byte[(int)chunk.length()];
			is.read(dataChunk);
			is.close();
		} catch (IOException e) {
		}
		return dataChunk;
	}


    private static class ChunkWriter extends Thread{
        BackupFile bf;
        byte[] chunk;

        public ChunkWriter(BackupFile bf, byte[] chunk){
            this.bf = bf;
            this.chunk = chunk;
        }
        synchronized public void run(){
            RestoreProtocol.chunkNo++;
            FileOutputStream out;
            try {
                out = new FileOutputStream(App.backupFolder.getAbsolutePath() + "/" + bf.getName(), true);
                out.write(chunk);
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(chunk.length == CHUNK_LENGTH){
                restoreFile(bf, RestoreProtocol.chunkNo );
            }
            else{
                System.out.println("Ficheiro restaurado");
            }

        }
    }
    public static synchronized boolean parseMessage(byte[] m){
        InputStream is = new ByteArrayInputStream(m);
        BufferedReader bfReader = new BufferedReader(new InputStreamReader(is));
        String header = null;
        try {
            header = bfReader.readLine();
        } catch (IOException e) {
            return false;
        }

        byte[] content = new byte[m.length - header.length() - 4];
        System.arraycopy(m, header.length() + 4, content, 0, m.length - header.length() - 4);

        Matcher cmatcher = CHUNK_PATTERN.matcher(header);

        if(cmatcher.matches()){
            String version = cmatcher.group(1);
            String uID = cmatcher.group(2);
            int chunkNo = Integer.parseInt(cmatcher.group(3));

            if(chunkMessages.containsKey(uID + "-" + chunkNo)){
                if(chunkMessages.get(uID + "-" + chunkNo) == true)
                    chunkMessages.put(uID + "-" + chunkNo, false);
                else
                    chunkMessages.put(uID + "-" + chunkNo, true);
            } else chunkMessages.put(uID + "-" + chunkNo, true);

            //System.out.println(chunkNo + " - true");
            if(App.hasLocalFile(uID) && chunkNo == RestoreProtocol.chunkNo){
                ChunkWriter cw = new ChunkWriter(App.getBackupFile(uID), content);
                cw.start();
                return true;
            }
        }

        return false;
    }

    private static class SendChunkWorker extends Thread{
        String uID;
        int chunkNo;

        public SendChunkWorker(String uID, int chunkNo){
            this.uID = uID;
            this.chunkNo = chunkNo;
        }

        synchronized public void run(){
            Random r = new Random();
            long interval = Math.abs(r.nextInt() % 400);
            Utils.sleep(interval);
            if(!chunkMessages.containsKey(uID + "-" + chunkNo)){
                chunkMessages.put(uID + "-" + chunkNo, false);
            }

            if(chunkMessages.get(uID + "-" + chunkNo) == false){
                byte[] chunk = getChunk(uID, chunkNo);
                if(chunk != null){
                    chunkMessages.put(uID + "-" + chunkNo, true);
                    byte[] message = generateChunk(chunk, uID, chunkNo);
                    App.mdrChannel.sendChunk(message);
                    return;

                }
            }
            else{
                //System.out.println("Ignore -" + chunkNo);
            }

            chunkMessages.put(uID + "-" + chunkNo, false);

        }
    }
    public static synchronized void sendChunk(String fileId, int chunkNo){
        SendChunkWorker scw = new SendChunkWorker(fileId, chunkNo);
        scw.start();
    }

    public static byte[] generateChunk(byte[] chunk, String uID, int chunkNo){
        String header = "CHUNK " + App.version + " " + uID + " " + chunkNo + "\r\n\r\n";
        byte[] headerB = header.getBytes();
        byte[] result = new byte[headerB.length + chunk.length];
        System.arraycopy(headerB, 0, result, 0, headerB.length);
        System.arraycopy(chunk, 0, result, headerB.length, chunk.length);
        return result;
    }

}
