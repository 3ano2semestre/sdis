package sdis.backup;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MCChannel extends Thread{

	/**
	 * @param args
	 */
	private String mcAddr;
	private String mcPort;
	
	public static MulticastSocket mcSocket;

    final private static Pattern STORED_PATTERN = Pattern.compile("^STORED (\\d\\.\\d) (.*) (\\d+)");
    final private static Pattern GETCHUNK_PATTERN = Pattern.compile("^GETCHUNK (\\d\\.\\d) (.*) (\\d+)" );
    final private static Pattern REMOVED_PATTERN = Pattern.compile("^REMOVED (\\d\\.\\d) (.*) (\\d+)");
    final private static Pattern DELETE_PATTERN = Pattern.compile("^DELETE (.*)");


    public MCChannel(String mcAddr, String mcPort){
		this.mcAddr = mcAddr;
		this.mcPort = mcPort;
		
		try {
			mcSocket = new MulticastSocket(Integer.parseInt(this.mcPort));
			mcSocket.setLoopbackMode(false);
			mcSocket.joinGroup(InetAddress.getByName(this.mcAddr));
            mcSocket.setTimeToLive(1);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public synchronized  void run(){
		String received;
		DatagramPacket packet;
		byte[] buf;
		
		while(true){
			buf = new byte[2048];
			packet = new DatagramPacket(buf, buf.length);
            String version;
            String fileId;
            String chunkNo;
            BackupFile bf;
			try {
				mcSocket.receive(packet);
				received = new String(packet.getData());
                String[] msplit = received.split("\r\n\r\n");
                Matcher storedMatcher = STORED_PATTERN.matcher(msplit[0]);
                if(storedMatcher.matches()){
                    version = storedMatcher.group(1);
                    fileId  = storedMatcher.group(2);
                    chunkNo = storedMatcher.group(3);
                    bf = App.getBackupFile(fileId);
                    if(bf != null){
                        bf.incCopies(Integer.parseInt(chunkNo));
                        Integer numCopies = bf.getCopies(Integer.parseInt(chunkNo));
                    }
                }

                Matcher getChunkMatcher = GETCHUNK_PATTERN.matcher(msplit[0]);
                if(getChunkMatcher.matches()){
                    version = getChunkMatcher.group(1);
                    fileId  = getChunkMatcher.group(2);
                    chunkNo = getChunkMatcher.group(3);
                    RestoreProtocol.sendChunk(fileId, Integer.parseInt(chunkNo));

                }


                Matcher removedMatcher = REMOVED_PATTERN.matcher(msplit[0]);

                if(removedMatcher.matches()){
                    version = removedMatcher.group(1);
                    fileId  = removedMatcher.group(2);
                    chunkNo = removedMatcher.group(3);
                    bf = App.getBackupFile(fileId);
                    bf.decCopies(Integer.parseInt(chunkNo));
                    if (!bf.hasCompleteFile()) {
                        ReclaimingSpaceProtocol.hasFile(bf, Integer.parseInt(chunkNo));
                    }
                }

                Matcher deleteMatcher = DELETE_PATTERN.matcher(msplit[0]);
                if(deleteMatcher.matches()){
                    fileId  = deleteMatcher.group(1);
                    bf = App.getBackupFile(fileId);

                    if(bf.getFile() == null){
                        DeleteProtocol.deleteFiles(bf);
                    }
                    App.removeBackupFile(fileId);
                }
			} catch (IOException e) {

			}
		}		
	}
	
	public void sendReceivedAck(String fileId,int chunkNo){

		String receivedAck = "STORED " + App.version + " "  + fileId + " " + chunkNo + "\r\n\r\n";
        byte[]rabuf = receivedAck.getBytes();
		DatagramPacket dataPacket;
		try {
			dataPacket = new DatagramPacket(rabuf, rabuf.length,InetAddress.getByName(mcAddr),Integer.parseInt(mcPort));
			mcSocket.send(dataPacket);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendDeleteMessage(String fileId){
		String deleteMessage = "DELETE " + fileId + "\r\n\r\n";
		byte[]dmbuf = deleteMessage.getBytes();
		DatagramPacket dataPacket;
		try {
			dataPacket = new DatagramPacket(dmbuf, dmbuf.length,InetAddress.getByName(mcAddr),Integer.parseInt(mcPort));
			mcSocket.send(dataPacket);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendRemovedChunkMessage(String fileId, int chunkNo){
		String removedChunkMessage = "REMOVED " + App.version + " "  + fileId + " " + chunkNo + "\r\n\r\n";
		byte[]rcmbuf = removedChunkMessage.getBytes();
		DatagramPacket dataPacket;
		try {
			dataPacket = new DatagramPacket(rcmbuf, rcmbuf.length,InetAddress.getByName(mcAddr),Integer.parseInt(mcPort));
			mcSocket.send(dataPacket);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendGetChunk(String fileId,int chunkNo){
		String getChunkMessage = "GETCHUNK " + App.version + " "  + fileId + " " + chunkNo +"\r\n\r\n";
		byte[]gcmbuf = getChunkMessage.getBytes();
		DatagramPacket dataPacket;
		try {
			dataPacket = new DatagramPacket(gcmbuf, gcmbuf.length,InetAddress.getByName(mcAddr),Integer.parseInt(mcPort));
			mcSocket.send(dataPacket);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
