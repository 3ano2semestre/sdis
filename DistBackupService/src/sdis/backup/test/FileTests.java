package sdis.backup.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import sdis.backup.BackupFile;
import sdis.backup.BackupProtocol;
import sdis.backup.RestoreProtocol;

public class FileTests {
	
	@Test
	public void testUniqueId(){
		BackupFile f =  new BackupFile(new File("/Users/pedro/Documents/FEUP/SDIS/aula1/.project"));
		String id = f.getUniqueId();
		assertNotNull("Not null string", id);
	}
	
	@Test
	public void testFileProperties(){
		File f =  new File("/Users/pedro/Documents/FEUP/SDIS/aula1/.project");
		f.lastModified();
	}
	
	@Test
	public void testBackupChunks(){
		BackupFile bf = new BackupFile(new File("/Users/pedro/Downloads/MAMP_2.2.pkg"));
		try {
			BackupProtocol.sendFile(bf);
		} catch (IOException e) {
			fail();
		}
	}
	
	@Test
	public void testRestoreFile(){
		RestoreProtocol.restore("6d11e7811d7e87c6a280f5a2e478b3b1fbfb08c1bb9af746961f42150f774cbd");
	}
}
