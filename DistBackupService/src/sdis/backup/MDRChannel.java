package sdis.backup;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

public class MDRChannel extends Thread{

	/**
	 * @param args
	 */
	private String mdrAddr;
	private String mdrPort;
	
	public static MulticastSocket mdrSocket;
	
	public MDRChannel(String mdrAddr, String mdrPort){
		this.mdrAddr = mdrAddr;
		this.mdrPort = mdrPort;
		
		try {
			mdrSocket = new MulticastSocket(Integer.parseInt(this.mdrPort));
			mdrSocket.joinGroup(InetAddress.getByName(this.mdrAddr));
            mdrSocket.setTimeToLive(1);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void run(){
		while(true){
			byte[] buf = new byte[70000];
			DatagramPacket packet = new DatagramPacket(buf, buf.length);
			try {
				mdrSocket.receive(packet);

                byte[] data = new byte[packet.getLength()];
                System.arraycopy(packet.getData(), 0, data, 0, packet.getLength());

                String received = new String(data);

                RestoreProtocol.parseMessage(data);

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}


    public void sendChunk(byte[] message){
        DatagramPacket dataPacket;
        try {
            dataPacket = new DatagramPacket(message, message.length,InetAddress.getByName(mdrAddr),Integer.parseInt(mdrPort));
            mdrSocket.send(dataPacket);
            //fazer contagem do numero de ack recebidos para saber se precisa de enviar novamente o pedido.
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
	public void sendRestoreChunk(String uniqueId, int chunkNo) {
		
		//procura chunk no pc e envia ( em vez de chunk info)
		String sendRestoreMessage = "CHUNK " + App.version + " "  + uniqueId + " " + chunkNo + " " + "chunk info";
		
		byte[] srmbuf = sendRestoreMessage.getBytes();
		
		DatagramPacket dataPacket;
		try {
			dataPacket = new DatagramPacket(srmbuf, srmbuf.length,InetAddress.getByName(mdrAddr),Integer.parseInt(mdrPort));
			mdrSocket.send(dataPacket);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
