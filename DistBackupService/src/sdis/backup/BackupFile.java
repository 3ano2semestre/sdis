package sdis.backup;

import java.io.File;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.Executor;

/**
 * Class that stores information about a file
 * As well as backup information (number of copies for chunks, etc).
 *
 */
public class BackupFile implements Serializable{
	
	/**
	 * Stores the current last modified timestamp when the app
	 * visits the file
	 */
	private long lastRecordedModified = 0;
	
	/**
	 * File itself
	 */
	private File file;
	
	/**
	 * Unique file ID
	 */
	private String uID;
	
	
	/**
	 * Replication Degree
	 */
	private int repDeg;

    /**
     * Original file name
     */
    private String name;

	/**
	 * Number of copies <chunkNo, classe Chunk NuberCopies>
	 */
	private HashMap<Integer, Chunk> copies = new HashMap<Integer, Chunk>();
	
	public BackupFile(File f) {
		this.file = f;
		this.lastRecordedModified = this.file.lastModified();
		this.uID = BackupFile.makeUniqueId(f.getName());
		this.repDeg = 1;
        this.name = f.getName();
	}
	public BackupFile(String uID, int repDeg){
        this.uID = uID;
        this.repDeg = repDeg;
        this.file = null;
    }

    /**
     * Sets replication degree of file
     * @param rd
     */
    public void setRepDeg(int rd){
        this.repDeg = rd;
    }

    /**
     * Get replication degree
     * @return
     */
	public int getRepDeg(){
		return this.repDeg;
	}
	
	public long getLastRecordedModified(){
		return this.lastRecordedModified;
	}
	public void setLastRecordedModified(long l){
		this.lastRecordedModified = l;
	}

	/**
	 * Creates unique string for a file
	 * @return Unique ID if successful, null otherwise
	 */
	public static String makeUniqueId(String name) {

		MessageDigest digest;
		String result = null;
		try {
			digest = MessageDigest.getInstance("SHA-256");
            name = name.concat(App.getUsername());
			byte[] hash = digest.digest(name.getBytes());
			StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < hash.length; i++) {
	        	sb.append(Integer.toString((hash[i] & 0xff) + 0x100, 16).substring(1));
	        }
	        result = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			result = null;
		}
        return result;
	}
	
	public String getUniqueId(){
		return this.uID;
	}
	
	public File getFile(){
		return this.file;
	}

    public void setStored(Integer chunkNo){
        Chunk chunk = copies.get(chunkNo);
        chunk.setStored(true);
    }

    public Collection<Chunk> getChunkArray(){
        return copies.values();
    }


    /**
     *
     * @return array chunks que estao stored
     */
    public ArrayList<Chunk> getChunkForRemove(){
        ArrayList<Chunk> res = new ArrayList<Chunk>();

        for(Chunk chunk : copies.values()){
            if (chunk.getStored()) {
                res.add(chunk);
            }
        }

        return res;

    }


    public synchronized Integer getCopies(Integer chunkNo){
        if (!copies.containsKey(chunkNo))
            this.copies.put(chunkNo, new Chunk(chunkNo));

        return this.copies.get(chunkNo).getNumCopies();
    }

    public synchronized void incCopies(Integer chunkNo){
        if (copies.containsKey(chunkNo)) {
            copies.get(chunkNo).incrementCopies();
        }
        else{
            Chunk c =new Chunk(chunkNo);
            c.incrementCopies();
            this.copies.put(chunkNo, c);
        }
    }

    public synchronized void decCopies(Integer chunkNo){

        if (copies.containsKey(chunkNo)) {
            copies.get(chunkNo).decrementCopies();
        }
        else{
            Chunk c =new Chunk(chunkNo);
            //c.decrementCopies();
            this.copies.put(chunkNo, c);

        }
    }

    public boolean hasCompleteFile(){
        if (file == null)
            return false;
        else
            return true;
    }

    public String getName(){
        return this.name;
    }



}
